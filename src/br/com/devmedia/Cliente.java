package br.com.devmedia;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Cliente extends javax.swing.JFrame {

    private Socket socket;
    private OutputStream os;
    private Writer w;
    private BufferedWriter bw;
    private JTextField txtIP;
    private JTextField txtPort;
    private JTextField txtName;
    private JLabel lbMessage;
    
    public Cliente() {
        setDataInit();
        initComponents();
        
        setTitle(txtName.getText());
        setVisible(true);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    private void setDataInit() {
        lbMessage = new JLabel("Verificar!");
        txtIP = new JTextField("127.0.0.1");
        txtPort = new JTextField("12345");
        txtName = new JTextField("Cliente");
        Object[] texts = {lbMessage, txtIP, txtPort, txtName};
        JOptionPane.showMessageDialog(null, texts);
    }

    private void conectar() {
        try {
            socket = new Socket(txtIP.getText(), Integer.parseInt(txtPort.getText()));
            os = socket.getOutputStream();
            w = new OutputStreamWriter(os);
            bw = new BufferedWriter(w);
            bw.write(txtName.getText() + "\r\n");
            bw.flush();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            setDataInit();
        }
    }
    
    private void enviarMensagem(String msg) throws IOException {
        if (msg.equals("Sair")) {
            bw.write("Desconectado \r\n");
            txtMsgs.append("Desconectado \r\n");
        } else {
            bw.write(msg + "\r\n");
            txtMsgs.append(txtName.getText() + " diz -> " + txtMsg.getText() + "\r\n");
        }
        bw.flush();
        txtMsg.setText("");
    }
    
    public void escutar() throws IOException {
        InputStream in = socket.getInputStream();
        InputStreamReader inr = new InputStreamReader(in);
        BufferedReader br = new BufferedReader(inr);
        String msg = "";
        
        while (!"Sair".equals(msg)) {
            if (br.ready()) {
                msg = br.readLine();
                if (msg.equals("Sair")) {
                    txtMsgs.append("Servidor caiu! \r\n");
                } else {
                    txtMsgs.append(msg + "\r\n");
                }
            }
        }
    }
    
    public void sair() throws IOException {
        enviarMensagem("Sair");
        bw.close();
        w.close();
        os.close();
        socket.close();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lbHistorico = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtMsgs = new javax.swing.JTextArea();
        lbMsg = new javax.swing.JLabel();
        txtMsg = new javax.swing.JTextField();
        btSair = new javax.swing.JButton();
        btEnviar = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lbHistorico.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbHistorico.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbHistorico.setText("Histórico");

        txtMsgs.setEditable(false);
        txtMsgs.setColumns(20);
        txtMsgs.setRows(5);
        jScrollPane1.setViewportView(txtMsgs);

        lbMsg.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbMsg.setText("Mensagem");

        txtMsg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMsgKeyPressed(evt);
            }
        });

        btSair.setText("Sair");
        btSair.setToolTipText("Sair do Chat");
        btSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSairActionPerformed(evt);
            }
        });

        btEnviar.setText("Enviar");
        btEnviar.setToolTipText("Enviar Mensagem");
        btEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEnviarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbHistorico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                    .addComponent(lbMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMsg)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btSair, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbHistorico)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbMsg)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMsg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSair)
                    .addComponent(btEnviar))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSairActionPerformed
        try {
            sair();
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btSairActionPerformed

    private void btEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEnviarActionPerformed
        try {
            enviarMensagem(txtMsg.getText());
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btEnviarActionPerformed

    private void txtMsgKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMsgKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                enviarMensagem(txtMsg.getText());
            } catch (IOException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_txtMsgKeyPressed

    public static void main(String args[]) throws IOException {
        Cliente app = new Cliente();
        app.conectar();
        app.escutar();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btEnviar;
    private javax.swing.JButton btSair;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbHistorico;
    private javax.swing.JLabel lbMsg;
    private javax.swing.JTextField txtMsg;
    private javax.swing.JTextArea txtMsgs;
    // End of variables declaration//GEN-END:variables
}